const mongoose = require('mongoose');
const Property = mongoose.model('Property');

exports.getProperties = async (req, res) => {
  // Query database for a list of all properties
  let properties = await Property.find().lean();
  res.render('index', {
    title: 'Property Management',
    properties
  });
};

exports.updateProperty = async (req, res) => {
  // Find and update the property
  const property = await Property.findOneAndUpdate({ _id: req.params.id }, req.body, {
    new: true, //return updated property
    runValidators: true,
  }).exec(function (err, prop) {
    if (prop) {
      return res.status(200).send(prop);
    }
    return res.status(500).send("There was a problem updating the property.");
  });
};

exports.addProperty = async (req, res) => {
  const property = await (new Property(req.body)).save()
    .then(prop => res.status(200).send(prop))
    .catch(err => res.status(500).send("There was a problem adding the property."));
};
