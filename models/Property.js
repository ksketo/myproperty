const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const propertySchema = new mongoose.Schema({
  owner: {
    type: String,
    trim: true,
    required: 'Please enter an owner name'
  },
  address: {
    line1: {
      type: String,
      trim: true,
      required: 'Please enter an owner name'
    },
    line2: String,
    line3: String,
    line4: {
      type: String,
      trim: true,
      required: 'Please enter an owner name'
    },
    postCode: {
      type: String,
      trim: true,
      required: 'Please enter the postcode of the property'
    },
    city: {
      type: String,
      trim: true,
      required: 'Please enter the city of the property'
    },
    country: {
      type: String,
      trim: true,
      required: 'Please enter the country of the property'
    }
  },
  incomeGenerated: {
    type: Number,
    trim: true,
    required: 'Please enter a number for the income of the property'
  },
  created: {
    type: Date,
    default: Date.now
  },
});

module.exports = mongoose.model('Property', propertySchema);
