require('dotenv').config({ path: __dirname + '/../variables.env' });
const fs = require('fs');

const mongoose = require('mongoose');
mongoose.connect(process.env.DATABASE);
mongoose.Promise = global.Promise; // Tell Mongoose to use ES6 promises

// import all of our models
const Property = require('../models/Property');

const properties = JSON.parse(fs.readFileSync(__dirname + '/exerciseData.json', 'utf-8'));

async function deleteData() {
  console.log('Removing data from Mongo...');
  await Property.remove();
  console.log('Data Deleted. To load sample data, run\n\n\t npm run migrate\n\n');
  process.exit();
}

async function loadData() {
  try {
    await Property.insertMany(properties);
    console.log('👍 Done! Data loaded');
    process.exit();
  } catch(e) {
    console.log('\n👎 Error! Make sure to drop existing database first with.\n\n\t npm run delete-db\n\n\n');
    console.log(e);
    process.exit();
  }
}

if (process.argv.includes('--delete')) {
  deleteData();
} else {
  loadData();
}
