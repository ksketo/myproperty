const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const Property = mongoose.model('Property');
const { catchErrors } = require('../handlers/errorHandlers');
const propertyController = require('../controllers/propertyController');


router.get('/', catchErrors(propertyController.getProperties))
router.post('/add', catchErrors(propertyController.addProperty));
router.post('/add/:id', catchErrors(propertyController.updateProperty));

module.exports = router;
