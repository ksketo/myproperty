const http = require('http');
const rp = require('request-promise');

const request = function(url) {
  return new Promise(resolve => {
    http.get({path: url}, response => {
      let data = '';
      response.on('data', _data => data += _data);
      response.on('end', () => resolve(data));
    });
  });
}

const options = {
  method: 'POST',
  uri: 'http://localhost/add',
  // port: 80,
  // path: '/add',
  body: {
    "owner": "kostas_new500",
    "address": {
        "line1": "Flat 5",
        "line4": "7 Westbourne Terrace",
        "postCode": "W2 3UL",
        "city": "London",
        "country": "U.K."
    },
    "incomeGenerated": 2000.34
  },
  json: true
};

describe('Test / endpoint', () => {
    it('returns table for GET method', () => {
        return request('/').then(data => {
          expect(data).toBeDefined()
          expect(data)
            .toEqual(expect.stringContaining('Owner'))
          expect(data)
            .toEqual(expect.stringContaining('kostas'))
        })
    })
});

describe('Test /add endpoint', () => {
    it('returns data for POST method', () => {
        return rp(options).then(function (data) {
          expect(data.owner).toEqual(options.body.owner);
          expect(data.address).toEqual(options.body.address);
          expect(data.incomeGenerated).toEqual(options.body.incomeGenerated);
        })
    })
    it('fails for empty required field', () => {
        let tmp_options = options;
        tmp_options.body.address['line1'] = ''
        return rp(tmp_options).catch(err => {
          expect(err.error).toEqual('There was a problem adding the property.');
        })
    })
});

describe('Test /add/:id endpoint', () => {
    it('updates data', () => {
        options.body.address['line1'] = 'Flat 5';
        return rp(options).then(function (data) {
            let id = data._id;
            tmp_options = options;
            tmp_options.uri = 'http://localhost/add/' + id;
            tmp_options.body.owner = 'new_name';
            return rp(tmp_options).then(function (data) {
              expect(data.owner).toEqual('new_name');
            })
        })
    })
    it('fails to update data', () => {
        return rp(options).then(function (data) {
            let id = data._id;
            tmp_options = options;
            tmp_options.uri = 'http://localhost/add/' + id;
            tmp_options.body.owner = '';
            return rp(tmp_options).catch(err => {
              expect(err.error).toEqual('There was a problem updating the property.');
            })
        })
    })
});
