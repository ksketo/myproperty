# MySearch

A property management mini app.

## How to run

```bash
docker-compose build

# Install npm dependencies
docker-compose run express bash
npm install

# Migrate JSON data to Mongo
npm run migrate

# Start express server
npm run watch

# To run tests
npm test
```

Npm version used is 5.0.3 and Node version is 8.1.2.
Server runs on port 80.

## Updating/Adding properties

To add a new property, make an API request to /add endpoint with the JSON content.
To update a property, make an API request to /add/:id endpoint with the JSON content.

Example of JSON content is:

```
{
	"owner": "new_owner",
	"address": {
	    "line1": "Flat 5",
	    "line4": "7 Westbourne Terrace",
	    "postCode": "W2 3UL",
	    "city": "London",
	    "country": "U.K."
	},
	"incomeGenerated": 2000.34
}
```

For making the POST requests you can use Postman

## To-Do

- Implement a better design for the UI
- Test front-end with Selenium (or add a front-end framework).
- Improve front-end to allow user to add and update properties by editing the table (without having to interact with the API directly)
- Add /delete/:id endpoint to the API, for removing properties.
- Implement document versioning to view history of changes in properties.
